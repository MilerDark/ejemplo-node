//importaciones
const { rainbow } = require('colors');
const argv = require('yargs').argv;
//metodos
const { generarArchivo } = require('./helpers/multiplicar')
//importando el paquete colors
require('colors')


// console.log(process.argv);
// console.log(argv);

console.log('base:yargs', argv.base);
  //const base = 5;
  // const [, , arg3 = 'base=9'] = process.argv;
  // const [, base] = arg3.split('=');
  // console.log(base);

  // generarArchivo(base)
  // .then(nombreArchivo => console.log(nombreArchivo, 'creado'))
  // .catch(err => console.log(err));


// console.log('===================');
// console.log(` Tabla del ${base} `);
// console.log('===================');

// const base = 5;
// // const base1 = 8;
// let salida = '';

// for(let i = 1; i <= 10; i++){
//   salida += `${base} x ${i} = ${base * i}\n`;
// }
// console.log(salida);
// //nombre del archivo primer parametro tabla-base.txt

// fs.writeFile(`tabla-${base}.txt`, salida, (err)=> {
//   if(err) throw err;
//   console.log(`table-${base} creado`);
// });


// // for(let i = 1; i <= 10; i++){
// //   console.log(`${base1} x ${i} = ${base1 * i}`);
// // }

generarArchivo(argv.base)
  .then(nombreArchivo => console.log(nombreArchivo.rainbow, 'creado'))
  .catch(err => console.log(err));